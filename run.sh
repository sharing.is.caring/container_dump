#!/bin/bash

DEBUG=true
CONTAINER_TXT="containers.txt"
#DEST_REG="registry.gitlab.com"
DEST_REG="$1"
REG_USER="$2"
REG_TOKEN="$3"
#DEST_PATH="sharing.is.caring/container_dump"
DEST_PATH="$4"
readarray -t arr2 < <(cat "${CONTAINER_TXT}")
echo $arr2[@]
for key in "${!arr2[@]}"; do
    if [[ "${arr2[$key]}" == *"#"* ]]; then 
       echo "skipping comment line..."
    else
        src_registry=$(echo "${arr2[$key]}" | awk -F "/" '{print $1=$1}')
        container_full=$(echo "${arr2[$key]}" | awk -F "/" '{$1=""; print $0}' | tr ' ' '/' )
        container=$(echo "$container_full" | awk -F ":" '{print $1=$1}' )
        tag=$(echo "$container_full" | awk -F ":" '{print $1=$2}' )
        dest_reg_image="$DEST_REG/${DEST_PATH}$container" 
        dest_full="${dest_reg_image}:${tag}"
        hashed=$(buildah pull "${arr2[$key]}")
        if [[ "$DEBUG" == "true" ]]; then
          echo $src_registry
          echo $container_full
          echo $container
          echo $tag
          echo $dest_reg_image
          echo $dest_container_full
          echo $hashed
        fi
    
    buildah login -u "${REG_USER}" -p "${REG_TOKEN}" "${DEST_REG}"
    buildah push ${hashed} ${dest_full}
    buildah logout registry.gitlab.com
  fi
done
